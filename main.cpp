// Copyright (C)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// 2021 - fjrg76 at hotmail dot com
//
// This project is based upon the shoulders of three awesome projects:
//
// 1.- Arduino ( https://www.arduino.cc )
// 2.- Arduino-mk ( https://github.com/sudar/Arduino-Makefile )
// 3.- FreeRTOS ( https://www.freertos.org )
//
// Each one might have its very own licensing schemes for logos, names and source
// code. Please visit their sites if in doubt.

#include <Arduino.h>

#include <FreeRTOS.h>
#include <task.h>

void software_delay( TickType_t ticks )
{
   volatile TickType_t now = xTaskGetTickCount();

   Serial.print( "\nBefore: " ); 
   Serial.println( now );

   volatile TickType_t next_wakeup = now + ticks;
   Serial.print( "Next: " ); Serial.println( next_wakeup );

   while( 1 )
   {
      if( now > next_wakeup ) 
      {
         break;
      } else
      {
         if( (now%100) == 0 )
         {
            Serial.println( now  );
         }
      }

      now = xTaskGetTickCount();
   }

   Serial.print( "After: " ); 
   Serial.println( now );
}

/*
 * High priority; retains the CPU
 */
void task1( void* pvParameters )
{
   (void) pvParameters;

   pinMode( 13, OUTPUT );

   while( 1 )
   {
      for( uint8_t i = 4; i > 0; --i )
      {
         vTaskDelay( pdMS_TO_TICKS( 1000 ) );
         // releases the CPU

//         software_delay( pdMS_TO_TICKS( 500 ) );
         // retains the CPU
      }
      Serial.println( "T1: retaining the CPU..." );

      digitalWrite( 13, HIGH );
      software_delay( pdMS_TO_TICKS( 2000 ) );
      digitalWrite( 13, LOW );
   }
}

/*
 * Low priority; it uses xTaskDelayUntil()
 */
void task0( void* pvParameters )
{
   (void) pvParameters;

   TickType_t last_wakeup = xTaskGetTickCount();

   pinMode( 2, OUTPUT );
   pinMode( 3, OUTPUT );
   digitalWrite( 2, LOW );
   digitalWrite( 3, LOW );

   while( 1 )
   {
      if( xTaskDelayUntil( &last_wakeup, pdMS_TO_TICKS( 1000 ) ) == pdFALSE )
      {
         digitalWrite( 2, LOW );
         digitalWrite( 3, HIGH );
         Serial.println( "T0: Perdí el deadline :(" );
      } else
      {
         digitalWrite( 2, HIGH );
         digitalWrite( 3, LOW );
         Serial.println( "T0: Ok :)" );
      }
   }
}


int main(void)
{
   cli();
   // disable interrupts while FreeRTOS is being set up

   init();
   // init the Arduino's hardware and stuff

   xTaskCreate( 
         task1,
         (const portCHAR *)"T1",
         512,
         ( void* ) 0,
         tskIDLE_PRIORITY + 1,
         NULL );

   xTaskCreate( 
         task0,
         (const portCHAR *)"T0",
         128,
         ( void* ) 0,
         tskIDLE_PRIORITY,
         NULL );

   Serial.begin( 115200 );
   Serial.println( "OUT OF RESET" );

   vTaskStartScheduler();
   // start the FreeRTOS scheduler

   while( 1 )
   {
      // nothing to do in here. Everything is done in the task's code
   }

   return 0;
}

#ifdef __cplusplus
extern "C"
{
#endif

   void vApplicationIdleHook()
   {
      // do something out of the tasks.
      // The code in here should NEVER block.
   }

   // something terrible has happened!!!
   void vApplicationMallocFailedHook()
   {
      cli();

      pinMode( 13, OUTPUT );

      while( 1 )
      {
         digitalWrite( 13, HIGH );
         for( size_t i = 65000; i > 0; --i);
         digitalWrite( 13, LOW );
         for( size_t i = 65000; i > 0; --i);
      }
   }

#ifdef __cplusplus
}
#endif

