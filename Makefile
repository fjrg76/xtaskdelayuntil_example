
ARDUINO_DIR = /home/fjrg76/arduino-1.8.13
BOARD_TAG = uno
ARDUINO_PORT = /dev/ttyUSB*
MONITOR_BAUDRATE = 115200
CXXFLAGS_STD = -std=gnu++11 -Wall -DUNO -Wpedantic -Wextra -g -Os
CFLAGS_STD = -std=gnu11 -Wall -Wpedantic -g -Os
CFLAGS_STD += -I$(ARDUINO_DIR)/libraries/FreeRTOS
CXXFLAGS_STD += -I$(ARDUINO_DIR)/libraries/FreeRTOS
CXXFLAGS_STD += -I$(ARDUINO_DIR)/libraries/FreeRTOS/os
#CFLAGS_STD += -DUSING_USER_TICK_HOOK=1
USER_LIB_PATH = /libraries
USER_LIB_PATH += /libraries/FreeRTOS
include /home/fjrg76/arduino-1.8.13/Arduino.mk

